﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Issoft.Meetups.EnumerableObserverSample
{
    public class EnumerableObserver<T> : IEnumerable<T>, IObserver<T>
    {
        #region Data

        private readonly IDisposable disposableToUnsubscribe;
        private readonly ConcurrentQueue<T> items = new ConcurrentQueue<T>();
        private readonly object completeLock = new object();
        private bool isCompleted;

        #endregion

        #region Helper classes

        private class ObservationErrorEventArgs : EventArgs
        {
            public ObservationErrorEventArgs(Exception error)
            {
                Error = error;
            }

            public Exception Error { get; private set; }
        }

        private class EnumerableObserverEnumerator : IEnumerator<T>
        {
            private readonly EnumerableObserver<T> observer;
            private T current;
            private bool isDisposed;
            private Exception observationError;

            private readonly object errorLock = new object();

            public EnumerableObserverEnumerator(EnumerableObserver<T> observer)
            {
                this.observer = observer;

                observer.OnObservationError += (o, args) =>
                {
                    lock (errorLock)
                    {
                        observationError = args.Error;
                    }
                };
            }

            public void Dispose()
            {
                isDisposed = true;
                observer.disposableToUnsubscribe.Dispose();
            }

            public bool MoveNext()
            {
                while (true)
                {
                    CheckDisposed();

                    lock (observer.completeLock)
                    {
                        if (observer.isCompleted) return false;
                    }

                    lock (errorLock)
                    {
                        if (observationError != null)
                        {
                            throw new Exception("Unhandled exception occured when observing, see InnerException for details", observationError);
                        }
                    }

                    if (observer.items.TryDequeue(out current))
                    {
                        return true;
                    }
                }
            }

            public void Reset()
            {
                throw new NotSupportedException("Resetting observation in not supported");
            }

            public T Current
            {
                get
                {
                    CheckDisposed();
                    return current;
                }
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            private void CheckDisposed()
            {
                if (isDisposed)
                {
                    throw new ObjectDisposedException("EnumerableObserverEnumerator");
                }
            }
        }

        #endregion

        #region Events

        private event Action<object, ObservationErrorEventArgs> OnObservationError;

        #endregion

        #region Factory

        public static IEnumerable<T> EnumerableObserverFor(IObservable<T> observableToObserve)
        {
            return new EnumerableObserver<T>(observableToObserve);
        }

        #endregion

        #region ctor

        private EnumerableObserver(IObservable<T> observableToObserve)
        {
            disposableToUnsubscribe = observableToObserve.Subscribe(this);
        }

        #endregion

        #region IEnumerable

        public IEnumerator<T> GetEnumerator()
        {
            return new EnumerableObserverEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IObserver

        public void OnNext(T value)
        {
            items.Enqueue(value);
        }

        public void OnError(Exception error)
        {
            if (OnObservationError != null) OnObservationError(this, new ObservationErrorEventArgs(error));
            disposableToUnsubscribe.Dispose();
        }

        public void OnCompleted()
        {
            lock (completeLock)
            {
                if (isCompleted) return;
                isCompleted = true;
            }
            disposableToUnsubscribe.Dispose();
        }

        #endregion
    }
}
