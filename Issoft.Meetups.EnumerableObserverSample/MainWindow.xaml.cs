﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Issoft.Meetups.EnumerableObserverSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IObservable<string>
    {
        #region ObservationStopper

        private class ObservationStopper : IDisposable
        {
            private readonly MainWindow window;
            private bool isDisposed;

            public IObserver<string> Observer { get; private set; }

            public ObservationStopper(MainWindow window, IObserver<string> observer)
            {
                this.window = window;
                this.Observer = observer;
            }

            public void Dispose()
            {
                if (isDisposed) return;
                isDisposed = true;
                Observer.OnCompleted();
                window.observers.Remove(this);
            }
        }

        #endregion

        #region Data

        private readonly List<ObservationStopper> observers = new List<ObservationStopper>();
        private readonly IEnumerable<string> items;
        private IEnumerable<IObserver<string>> Observers
        {
            get { return observers.Select(o => o.Observer); }
        }

        #endregion

        #region ctor

        public MainWindow()
        {
            InitializeComponent();
            items = EnumerableObserver<string>.EnumerableObserverFor(this);
            ToggleSecondaryControlsVisibility();
        }

        #endregion

        #region IObservable

        public IDisposable Subscribe(IObserver<string> observer)
        {
            var result = new ObservationStopper(this, observer);
            observers.Add(result);
            return result;
        }

        #endregion

        #region Click handlers

        private void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            var text = TextBox.Text;

            if (string.IsNullOrWhiteSpace(text)) return;

            foreach (var observer in Observers)
            {
                observer.OnNext(text);
            }
        }

        private void StartIteratingButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleControlVisibility(StartIteratingButton);
            Task.Run(() =>
            {
                try
                {
                    foreach (var item in items)
                    {
                        var item1 = item;
                        ListBox.Dispatcher.Invoke(() => ListBox.Items.Add(item1));
                    }
                }
                catch (Exception exception)
                {
                    ListBox.Dispatcher.Invoke(
                        () => ListBox.Items.Add(string.Format("Error {0}, stopping observations", exception.Message)));
                }
                finally
                {
                    ListBox.Dispatcher.Invoke(
                        () => ListBox.Items.Add("Observations complete!"));
                }
            });
            ToggleSecondaryControlsVisibility();
        }

        private void ErrorButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var observer in Observers.ToList())
            {
                observer.OnError(new Exception(ErrorBox.Text));
            }
            ToggleRecordControlsVisibility();
            ToggleSecondaryControlsVisibility();
        }

        private void StopIteratingButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var observer in Observers.ToList())
            {
                observer.OnCompleted();
            }
            ToggleRecordControlsVisibility();
            ToggleSecondaryControlsVisibility();
        }

        #endregion

        #region Helpers

        private void ToggleSecondaryControlsVisibility(Visibility? visibility = null)
        {
            ToggleControlVisibility(ErrorBox, visibility);
            ToggleControlVisibility(ErrorButton, visibility);
            ToggleControlVisibility(StopIteratingButton, visibility);
        }

        private void ToggleRecordControlsVisibility(Visibility? visibility = null)
        {
            ToggleControlVisibility(TextBox, visibility);
            ToggleControlVisibility(RecordButton, visibility);
        }

        private static void ToggleControlVisibility(UIElement control, Visibility? visibility = null)
        {
            control.Visibility = visibility ??
                                 (control.Visibility == Visibility.Hidden
                                     ? Visibility.Visible
                                     : control.Visibility == Visibility.Visible
                                         ? Visibility.Hidden
                                         : Visibility.Collapsed);
        }

        #endregion
    }
}
