﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Issoft.Meetups.BuilderWithComplexStatesExample;

namespace IssoftMeetupsBuilderWithComplexStatesUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new BuilderBase();

            builder

                //.Build()

                .AddSomePart()

                .AddPart1()
                
                //.Build()

                .AddSomePart()
                .AddPart2()

                .AddPart2()
                .AddPart1()
                .AddSomePart()

                .Build();
        }
    }
}
