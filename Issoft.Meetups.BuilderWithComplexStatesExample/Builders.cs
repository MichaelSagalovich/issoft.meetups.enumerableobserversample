﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Issoft.Meetups.BuilderWithComplexStatesExample
{
    public interface IBuilder<out TBuilderBase, out TBuilderState1, out TBuilderState2>
        where TBuilderBase : IBuilder<TBuilderBase, TBuilderState1, TBuilderState2>
        where TBuilderState1 : IBuilder<TBuilderState1, TBuilderState1, TBuilderState2>
        where TBuilderState2 : IBuilder<TBuilderState2, TBuilderState1, TBuilderState2>
    {
        TBuilderBase AddSomePart();
        TBuilderState1 AddPart1();
        TBuilderState2 AddPart2();

    }

    public interface IBuilderState1 : IBuilder<IBuilderState1, IBuilderState1, BuilderFinal>
    {
    }

    public interface IBuilderState2 : IBuilder<IBuilderState2, BuilderFinal, IBuilderState2>
    {
    }

    public class BuilderBase : IBuilder<BuilderBase, IBuilderState1, IBuilderState2> 
    {
        public BuilderBase AddSomePart()
        {
            return this;
        }

        public IBuilderState1 AddPart1()
        {
            return new BuilderState1(this);
        }

        public IBuilderState2 AddPart2()
        {
            return new BuilderState2(this);
        }
    }

    internal class BuilderState1 : IBuilderState1
    {
        internal BuilderState1(BuilderBase builderBase)
        {
        }

        public IBuilderState1 AddSomePart()
        {
            return this;
        }

        public IBuilderState1 AddPart1()
        {
            return this;
        }

        public BuilderFinal AddPart2()
        {
            return new BuilderFinal(this);
        }
    }

    internal class BuilderState2 : IBuilderState2
    {
        internal BuilderState2(BuilderBase builderBase)
        {
        }

        public IBuilderState2 AddSomePart()
        {
            return this;
        }

        public BuilderFinal AddPart1()
        {
            return new BuilderFinal(this);
        }

        public IBuilderState2 AddPart2()
        {
            return this;
        }
    }

    public class BuilderFinal : IBuilderState1, IBuilderState2, IBuilder<BuilderFinal, BuilderFinal, BuilderFinal>
    {
        internal BuilderFinal(BuilderState2 builderState2)
        {
        }

        internal BuilderFinal(BuilderState1 builderState1)
        {
        }

        public void Build()
        {
        }

        public BuilderFinal AddPart1()
        {
            return this;
        }

        public BuilderFinal AddPart2()
        {
            return this;
        }

        public BuilderFinal AddSomePart()
        {
            return this;
        }

        IBuilderState1 IBuilder<IBuilderState1, IBuilderState1, BuilderFinal>.AddPart1()
        {
            return AddPart1();
        }

        BuilderFinal IBuilder<IBuilderState1, IBuilderState1, BuilderFinal>.AddPart2()
        {
            return AddPart2();
        }

        IBuilderState1 IBuilder<IBuilderState1, IBuilderState1, BuilderFinal>.AddSomePart()
        {
            return AddSomePart();
        }

        BuilderFinal IBuilder<IBuilderState2, BuilderFinal, IBuilderState2>.AddPart1()
        {
            return AddPart1();
        }

        IBuilderState2 IBuilder<IBuilderState2, BuilderFinal, IBuilderState2>.AddPart2()
        {
            return AddPart2();
        }

        IBuilderState2 IBuilder<IBuilderState2, BuilderFinal, IBuilderState2>.AddSomePart()
        {
            return AddSomePart();
        }
    }
}
